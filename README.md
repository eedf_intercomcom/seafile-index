# seafile-index : création de l'index d'une bibliothèque Seafile

*english version below*

Seafile-index est un script python3 qui génère un index d'une bibliothèque 
Seafile sous forme de fichier Markdown, et le mets à jour dans la bibliothèque en question.

Il peut ainsi être appelé par une tâche cron pour mettre à jour régulièrement cet index.

Ce script a été créé pour les besoin du serveur [Galilée](https://galilee.eedf.fr/) des 
[EEDF](https://eedf.fr/), et est disponible selon les termes de la licence AGPLv3+.

## Utilisation

 1. récupérer un token d'identification pour le compte seafile utilisé :
 
    ./seafile-index mail@example.com https://seafile.server/ --password mypass

 2. lancer l'indexation avec le token en question :
 
    ./seafile-index mail@example.com https://seafile.server/ \
        --token 6dcfbe8e754abcdeedd9449b552de0fc1eb450ff
        --lib NomDeLaBibliothèque
        --tpl ../tpl/index-tpl_fr.md
        --idx NomDuFichierIndex.md

# ENGLISH : seafile-index: create an index of a Seafile library

Seafile-index is a python3 script wich generate a seafile library index under the form
of a Markdown file, and put it in the corresponding library.

It can be run by a cron job to update the index.

This script was created for the needs of the [Galilée](https://galilee.eedf.fr/) 
([EEDF](https://eedf.fr/)), and is available under the terms of the AGPLv3+ license.

## Usage

 1. get the authentication token for the seafile account you want to use:
 
    ./seafile-index mail@example.com https://seafile.server/ --password mypass

 2. run the indexer with the given token:

    ./seafile-index mail@example.com https://seafile.server/ \
        --token 6dcfbe8e754abcdeedd9449b552de0fc1eb450ff
        --lib LibraryName
        --tpl ../tpl/index-tpl_fr.md
        --idx NameOfTheIndexFile.md

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       seafile-index/seafile-index.py
#       
#       Copyright © 2015-2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafile-index.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafile indexer"""

__author__ = "Florence Birée"
__version__ = "0.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2015-2020, Florence Birée <florence@biree.name>"

import os
import sys
import argparse
# import de python-seafile-webapi (variable SEAFWEBAPI de env)
sys.path.insert(0, os.environ['SEAFWEBAPI'])
from seafile_webapi import SeafileClient, FileExists
from jinja2 import Environment, FileSystemLoader
from io import StringIO
from urllib.parse import quote

class SeafileTokenGenerator:
    """This helper class allow users to get a token from a password"""
    
    def __init__(self, base_url, email, password):
        """Connect to seafile, and return an authentication token"""
        self.api = Seafile(base_url)
        self.api.authenticate(email, password)
        self.token = self.api.token

class SeafileIndexer:
    """Main class of the seafile library indexer."""
    
    def __init__(self, base_url, email, token, repo, tpl_file, idx_filename):
        """Initialize a new SeafileIndexer
        """
        self.api = SeafileClient(base_url)
        self.api.authenticate(email, token=token, validate=True)
        self.repo_name, self.repo_id = None, None
        
        # find the repo ID
        repo_list = self.api.list_repos()
        for repo_st in repo_list:
            if repo_st['name'] == repo or repo_st['id'] == repo:
                self.repo_id = repo_st['id']
                self.repo_name = repo_st['name']
                break
        
        if self.repo_id is None:
            print("Library {0} not found.".format(repo), file=sys.stderr)
            sys.exit(2)
            #TODO: use exceptions
        
        index = self.build_index('/')
        index_file_content = self.build_index_file(index, tpl_file)
        self.update_index(index_file_content, idx_filename)
    
    def _interal_link(self, path, isdir):
        """Return an internal link for path"""
        if isdir:
            return self.api.internal_dir_link(self.repo_id, path)
        else:
            return self.api.internal_file_link(self.repo_id, path)
    
    def build_index(self, path):
        """Return the given index under the form of:
            [
            {'isdir': False, 'name': 'name1', 'path': '/name1', 'iurl': ''},
            {'isdir': True, 'name': 'name2', 'path': '/name2', 'iurl': '', content: [
                {isdir …
            ]},
            …
            ]
            
            iurl is an internal url link
        """
        content = self.api.list_dir(self.repo_id, path)
        index = []
        for item in content:
            if not item['name'].startswith('.'):
                if item['type'] == 'dir':
                    index.append({
                        'isdir': True,
                        'name': item['name'],
                        'path': path + '/' + item['name'],
                        'iurl': self._interal_link(path + '/' + item['name'], True),
                        'content': self.build_index(path + '/' + item['name'])
                    })
                elif item['type'] == 'file':
                    index.append({
                        'isdir': False,
                        'name': item['name'],
                        'path': path + '/' + item['name'],
                        'iurl': self._interal_link(path + '/' + item['name'], False)
                    })
        
        return index
    
    def build_index_file(self, index, tpl_file):
        """Create the index file by filling the template with an index"""
        env = Environment(
            line_statement_prefix = '#{',
            loader=FileSystemLoader(os.path.dirname(tpl_file), followlinks=True)
        )
        template = env.get_template(os.path.basename(tpl_file))
        content = template.render(
            repo_name=self.repo_name,
            repo_url=self._interal_link('/', isdir=True),
            content=index
        )
        return content.replace('\n\n', '\n')
    
    def update_index(self, index_file_content, index_filename):
        """Update the index in the library"""
        if not index_filename.startswith('/'):
            index_filename = '/' + index_filename
        fileo = StringIO(index_file_content)
        fileo.name = index_filename
        try:
            self.api.update_file(self.repo_id, index_filename, fileo)
        except FileExists:
            parent = os.path.dirname(index_filename)
            fileo = StringIO(index_file_content)
            fileo.name = index_filename
            self.api.upload_file(self.repo_id, parent, fileo)

if __name__ == '__main__':
    # parse command line arguments, and start the indexer
    
    parser = argparse.ArgumentParser()
    parser.add_argument("EMAIL", help="e-mail of your Seafile account")
    parser.add_argument("--password", help="password of your account, will \
                        return an unique token and exit")
    parser.add_argument("--token", help="token to authenticate instead of \
                        password")
    parser.add_argument("BASE_URL", help="url of the Seafile server")
    parser.add_argument("--lib", help="name or id of the Seafile library")
    parser.add_argument("--tpl", help="template file to use for the index")
    parser.add_argument("--idx", help="index filename")
    args = parser.parse_args()
    
    if args.password:
        stg = SeafileTokenGenerator(args.BASE_URL, args.EMAIL, args.password)
        print("Your token is: {0}".format(stg.token))
    elif args.token:
        if not args.lib or not args.tpl or not args.idx:
            print("You must provide --lib, --tpl and --dix arguments.",
                    file=sys.stderr)
            sys.exit(3)
        si = SeafileIndexer(args.BASE_URL, args.EMAIL, args.token, args.lib,
                            args.tpl, args.idx)
    else:
        print("You must either provide --password or --token.", file=sys.stderr)
        sys.exit(1)
    
    

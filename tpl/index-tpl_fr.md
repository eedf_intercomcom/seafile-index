# Index de [{{ repo_name }}]({{ repo_url }})

Cette page est mise à jour une fois tous les 24h.

Pour faire une recherche dans ce fichier : CTRL+F

------------

{% for item in content recursive -%} 
{%- if item.isdir -%}
{{ '  ' * loop.depth0 }}- 📂 [**{{ item.name }}**]({{ item.iurl }})
{{ loop(item.content) }}
{% else -%} 
{{ '  ' * loop.depth0 }}- 📄 [{{ item.name }}]({{ item.iurl }})
{% endif -%}
{%- endfor -%} 

